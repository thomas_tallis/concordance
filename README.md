# concordance

## Overview

Generate a list of unique words from input text file, sorted by word count.

### Requirements

* Java v11 or greater
* Gradle

Java and Gradle can be installed in various ways. Search SO (Stack Overflow) or
other technical resources.

## Build Project

From the project's root directory:

```bash
$ ./gradlew assemble
```

To see the available Gradle tasks, run the following:

```bash
$ ./gradlew tasks
```

During development, you can run the following command to both compile and then execute
the application. Replace <filename> with a real filename (absolute path).

```bash
$ ./gradlew run --args="-f <filename> -m <min word count>"
```

## Make fat jar

This is a gradle feature, using the "com.github.johnrengelman.shadow" plugin; it
creates a single jar file containing all of the project's dependencies.

```bash
$ ./gradelew jar
```

## Execute application

After the fat jar has been created, you can then run the program directly
against the jar. The fat jar, in this case.

The following example uses the required `-f` command line option to specify the
full path of a filename as input. The file should be a text file, such as a
Gutenberg ebook in plaintext format. Try Walden by Thoreau if you want:

#### https://www.gutenberg.org/files/205/205-0.txt

An optional command line switch is `-m` followed by an integer. This stands for
minimum word count, which will display only words in which the count is at least
the amount you specify.

### Example

The following example reads in Bram Stoker's famous gothic novel and generates a
concordance. The concordance displays to the console the list of words with
counts. The sort order is by count, descending, i.e., the most common words at
the top.

The following command will only display words in which the count >= 100.

```bash
$ java -cp $PWD/build/libs/concordance.jar gold.gerry.concordance.App -f /home/gold/books/dracula.txt -m 100
```

Have Fun!

Gerry Gold, February 2020
