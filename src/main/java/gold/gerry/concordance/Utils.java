package gold.gerry.concordance;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Utils {

  final private static String STOP_WORDS_FILENAME = "src/main/java/gold/gerry/concordance/stop-words.txt";

  public Map<String, Boolean> initStopWords() {
    final Map<String, Boolean> stopWords = new HashMap<String, Boolean>();
    BufferedReader bReader = null;

    try {
      bReader = getBufferedReader(STOP_WORDS_FILENAME);
      String stopWord = "";
      while ((stopWord = bReader.readLine()) != null) {
        if (stopWord.length() > 0) {
          stopWords.put(stopWord, true);
        }
      }
      bReader.close();
    } catch (final IOException e) {
      try {
        if (bReader != null) {
          bReader.close();
        }
      } catch (final Exception e2) {
        e2.printStackTrace();
        System.exit(2);
      }
      e.printStackTrace();
      System.exit(1);
    }

    return stopWords;
  }

  public Map<String, Integer> sortHashMapByValue(final HashMap<String, Integer> map, final String order) {
    // convert HashMap into List
    final List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(map.entrySet());

    Collections.sort(list, new Comparator<Entry<String, Integer>>() {
      public int compare(final Entry<String, Integer> o1, final Entry<String, Integer> o2)  {
        if (order == "asc") {
          return o1.getValue().compareTo(o2.getValue());
        } else {
          return o2.getValue().compareTo(o1.getValue());
        }
      }
    });

    final Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
    for (final Entry<String, Integer> entry : list) {
      sortedMap.put(entry.getKey(), entry.getValue());
    }

    return sortedMap;
  }

  public static BufferedReader getBufferedReader(final String filename) throws FileNotFoundException {
    final File fileHandle = new File(filename);
    return new BufferedReader(new FileReader(fileHandle));
  }
}
