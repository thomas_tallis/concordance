package gold.gerry.concordance;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class Concordance {

  public Integer minCount;
  private final Utils utils;
  private final Map<String, Boolean> stopWords;
  private final HashMap<String, Integer> wordsMap;

  public Concordance() {
    utils = new Utils();
    stopWords = utils.initStopWords();
    wordsMap = new HashMap<String, Integer>();
    minCount = 0;
  }

  public void runConcordance(final String filename, final Integer minCount) throws IOException {
    this.minCount = minCount;

    try {
      final BufferedReader bReader = Utils.getBufferedReader(filename);

      String readLine = "";
      while ((readLine = bReader.readLine()) != null) {
        final String words[] = parseLine(readLine);
        populateMap(words);
      }

      bReader.close();

      final Map<String, Integer> sortedWords = sortByWordCount();
      displayResults(sortedWords);

    } catch (final IOException e) {
      System.out.println(e.getMessage());
    }
  }

  public void displayResults(final Map<String, Integer> sortedWords) {
    for (final Map.Entry<String, Integer> entry : sortedWords.entrySet()) {
      final String key = entry.getKey();
      final int value = entry.getValue();
      if (value >= this.minCount) {
        out(key + ": " + value);
      }
    }
  }

  private void populateMap(final String[] words) {
    for (final String word : words) {

      // lowercase to be case insensitive and remove apostrophes to catch
      // contractions
      final String lcWord = word.toLowerCase().replaceAll("'", "");

      if (word.length() <= 1) {
        continue;
      }

      if (stopWords.containsKey(lcWord)) {
        continue;
      }

      if (wordsMap.containsKey(lcWord)) {
        wordsMap.computeIfPresent(lcWord, (k, v) -> v + 1);
      } else {
        wordsMap.put(lcWord, 1);
      }

    }
  }

  private Map<String, Integer> sortByWordCount() {
    final Map<String, Integer> sortedWords = utils.sortHashMapByValue(wordsMap, "desc");
    return sortedWords;
  }

  private String[] parseLine(final String readLine) {
    // Comma is included to capture words that are contractions. Common
    // contractions are inlcuded in the stop words list, so they'll be removed
    // from the concordance. Ironically, the contractions must first be included
    // so they can be recognized and then be subsequently removed. Otherwise,
    // words like the following would appear in the cocordance: don, won, t, ve
    final String WORD_DELIMITER = "[^a-zA-Z-']+";

    final String line = new String(readLine);
    final String words[] = line.split(WORD_DELIMITER);
    return words;
  }

  private static void out(final String s) {
    System.out.println(s);
  }
}
