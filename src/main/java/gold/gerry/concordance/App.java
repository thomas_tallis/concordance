/**
 * Development:
 *
 *   From project's root directory:
 *
 *   $ ./gradlew run --args="-f <filename> -m <min word count>"
 *
 * Make fat jar:
 *
 *   $ ./gradelew jar
 *
 * Execute application:
 *
 *   $ java -cp $PWD/build/libs/concordance.jar gold.gerry.concordance.App -f /path/to/file.txt [-m 100]
 */

package gold.gerry.concordance;

import java.io.IOException;
import java.util.Arrays;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

import gold.gerry.concordance.Concordance;

public class App {
  public String getGreeting() {
    return "Hello Concordance.";
  }

  public static void main(final String[] args) {
    final Concordance c = new Concordance();

    String filename = "";
    Integer minCount = 0;

    final OptionParser optionParser = new OptionParser();

    final String[] fileOptions = { "f", "file" };
    optionParser.acceptsAll(Arrays.asList(fileOptions), "Path and name of file.").withRequiredArg().required();

    final String[] minCountOptions = { "m", "mincount" };
    optionParser.acceptsAll(Arrays.asList(minCountOptions), "Minimum word count to display").withRequiredArg();

    try {
      // filename is required
      final OptionSet options = optionParser.parse(args);
      filename = (String) options.valueOf("file");

      // mincount is optional
      if (options.has("mincount")) {
        final String m = (String) options.valueOf("mincount");
        minCount = Integer.parseInt(m);
      }

    } catch (final Exception e) {
      System.out.println("Missing file name, e.g.: -f /path/to/file.txt or invalid minimum count");
      System.exit(1);
    }

    try {
      c.runConcordance(filename, minCount);
    }
    catch(IOException e) {
      e.printStackTrace();
    }
  }
}
